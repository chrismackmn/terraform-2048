variable "workspace_iam_roles" {
  default = {
    stage = "arn:aws:iam::STAGING-ACCOUNT-ID:role/TerraformECSServiceRole"
    prod  = "arn:aws:iam::741296337486:role/TerraformECSServiceRole"
  }
}
